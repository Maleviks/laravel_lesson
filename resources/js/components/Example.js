import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './style.css';

export default class Example extends Component {
    constructor(props){
        super(props);
        this.state = {
            header: '',
            show: '',
            products: [],
            names: []
        }
        this.changeValue = this.changeValue.bind(this);
        this.showResult = this.showResult.bind(this);
        this.changename = this.changename.bind(this);
        this.updateValueLaravel = this.updateValueLaravel.bind(this);
    }
    changeValue(e){
        this.setState({header: e.target.value});
    }

    showResult(){
        this.setState({show: this.state.header});
    }

    componentDidMount() {
        fetch('/api/products')
        .then(response => {
            return response.json();
        })
        .then(products => {
            let names = this.state.names;
            products.map(element => names[element.id] = element.name);
            this.setState({
                names: names, 
                products: products
            });
        });
    }

    changename(id, e){
        let names = this.state.names;
        names[id] = e.target.value;
        this.setState({names: names});
    }

    updateValueLaravel(id){
        let names = this.state.names;
        let name = names[id];
        let product = {
            id: id,
            name: name
        }
        fetch('/api/product_update',
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(product)
        })
        .then(response => {
            return response.json();
        })

        .then(product => {
            product = JSON.parse(product);
            let products = this.state.products;
            let names = this.state.names;
            for(let i in products){
                if(products[i].id == product['id']){
                    products[i].name = product['name'];
                }
            }
            this.setState({
                names: names,
                products: products
            });
            console.log('product', product)
        });
    }

    render() {
        let show = this.state.show;
        let header = this.state.header;
        let names = this.state.names;
        let products = this.state.products.map(element => {
            return(
            <div className="product-card" key={element.id}>
                <span className="product-card__name">{element.id}. {element.name}</span>
                <span className="product-card__description">{element.description}</span>
                <span className="product-card__price">{element.price} руб.</span>
                <div className="product-card__changer">
                    <input type="text" value={names[element.id]} onChange={(e) => {this.changename(element.id, e)}}/>
                    <button onClick={(e) => {this.updateValueLaravel(element.id, e)}}>Поменять название</button>
                </div>
            </div>)}
        );
        return (
            <div className="container">
                <input type="text" value={header} onChange={this.changeValue}/>
                <button onClick={this.showResult}>Показать результат</button>
                <h1 className="title m-b-md">{show}</h1>
                <h2>Данные из Ларавеля</h2>
                <div className="products-container">{products}</div>
            </div>
        );
    }
}