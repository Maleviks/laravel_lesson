<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
    	dd(Task::all());
    }

    public function getProducts()
    {
    	return Product::all()->toJson();
    }

    public function updateProduct(Request $request)
    {
    	$id = $request->id;
    	$name = $request->name;
    	$product = Product::where('id', $id)->first();
    	$product->name = $name;
    	$product->update();
    	return json_encode($product->toJson());
    }

}
