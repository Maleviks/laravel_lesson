<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Task;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
    	dd(Task::all());
    }

    public function getTasks()
    {
    	return Task::all()->toJson();
    }

    public function updateTask(Request $request)
    {
    	$id = $request->id;
    	$title = $request->title;
    	$task = Task::where('id', $id)->first();
    	$task->title = $title;
    	$task->update();
    	return json_encode($task->toJson());
    }

}
